var findPath = function(graph, startNode, endNode, onComplete, options) {
	
	var open = [];
	var closed = [];
	var processed = 0;
	
	var time = Date.now();
	var trace = [];
	
	var EMPTY = null;
	var options = options || { debug: false };

	function getNeighbours(node) {

		var x = node.x;
		var y = node.y;

		var neighbours = [];

		if (graph[y-1] && graph[y-1][x] === EMPTY) {
			neighbours.push({ x: x, y: (y-1) });
		}

		if (graph[y][x+1] === EMPTY) {
			neighbours.push({ x: (x+1), y: y });
		}

		if (graph[y+1] && graph[y+1][x] === EMPTY) {
			neighbours.push({ x: x, y: (y+1) });
		}

		if (graph[y][x-1] === EMPTY) {
			neighbours.push({ x: (x-1), y: y });
		}

		return neighbours;
	}

	function searchBestNode(array) {

		var index = 0;
		var value = array[index].f;

		for (var i = 1, len = array.length; i < len; i++) {
			if (array[i].f < value) {
				index = i;
				value = array[index].f;
			}
		}

		return index;
	}

	/*function searchBestNode(array) {

		var best = 0;

		for (var i = 1, len = array.length; i < len; i++) {
			if (array[i].f < array[best].f) {
				best = i;
			}
			if ((array[i].f === array[best].f) && (array[i].h < array[best].h)) {
				best = i;
			}
		}

		return best;
	}*/

	function getNextNode() {

		if (!open.length) {
			
			return onComplete(false);
		}

		processed++;

		// get node with lowest F from open list
		var currentNodeIndex = searchBestNode(open);

		// remove node from open list
		var currentNode = open.splice(currentNodeIndex, 1)[0];

		// add node to closed list
		trace[currentNode.y][currentNode.x].closed = true;

		// if this node is endNode node
		if ((currentNode.x === endNode.x) && (currentNode.y === endNode.y)) {

			time = Date.now() - time;

			var path = [], x = currentNode.x, y = currentNode.y;

			path.push({ x: x, y: y });

			for (var i = 0; i < currentNode.g; i++) {
				
				var _x = trace[y][x].parent.x, _y = trace[y][x].parent.y;
				
				path.push({ x: _x, y: _y });
				
				x = _x, y = _y;
				
				if (options.debug) {
					clearNode(y, x);
					drawNode(y, x, '#7FFF00');
				}
			}

			return onComplete({ path: path.reverse(), time: time, processed: processed });

		} else {

			var neighbours = getNeighbours(currentNode);

			for (var n = 0; n < neighbours.length; n++) {
				
				var neighbour = neighbours[n];

				if ( !trace[neighbour.y][neighbour.x].closed ) {
					
					neighbour.g = currentNode.g + 1;
					neighbour.h = Math.abs(neighbour.x - endNode.x) + Math.abs(neighbour.y - endNode.y);
					neighbour.f = neighbour.g + neighbour.h;
					neighbour.parent = currentNode;
					
					neighbour.closed = false;

					trace[neighbour.y][neighbour.x] = neighbour;

					var index = null;

					for (var i = 0; i < open.length; i++) {
						if ((open[i].x === neighbour.x) && (open[i].y === neighbour.y)) {
							index = i;
							break;
						}
					}

					if (index != null) {
						// console.log(`node already in open list, checking...`);
						if (neighbour.g < open[index].g) {
							// console.log(`updating node y${open[index].y}x${open[index].x}`);
							open[index].g = neighbour.g;
							open[index].f = neighbour.g + neighbour.f;
						}
					} else {
						
						open.push(neighbour);
						
						if (options.debug) {
							drawNode(neighbour.y, neighbour.x, '#7FFFD4');
						}
					}
				}
			}

			if (options.debug) {
				
				clearNode(currentNode.y, currentNode.x);
				drawNode(currentNode.y, currentNode.x, '#DC143C');
				
				window.setTimeout(function(){
					
					getNextNode();

				}, 0);

			} else {

				getNextNode();
			}
		}
	}

	function drawNode(y, x, color) {
		ctx.fillStyle = color;
		ctx.beginPath();
		ctx.arc(x*unit+(unit/2), y*unit+(unit/2), (unit/3), 0, Math.PI*2,true);
		ctx.fill();
	}

	function clearNode(y, x) {
		ctx.clearRect(x*unit+1, y*unit+1, unit-2, unit-2);
	}

	function start() {
			
		for (var y = 0; y < graph.length; y++) {
			
			trace[y] = [];

			for (var x = 0; x < graph.length; x++) {
				
				var node = {};
				
				node.f = null;
				node.g = null;
				node.h = null;
				node.cost = 1;
				node.visited = false;
				node.closed = false;
				node.parent = null;

				node.value = graph[y][x];
				trace[y][x] = node;
			}
		}

		startNode.g = 0;
		open.push(startNode);
		getNextNode();
	}

	start();
}
